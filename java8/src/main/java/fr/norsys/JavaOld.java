package fr.norsys;

import fr.norsys.modele.Bar;
import fr.norsys.modele.biere.Biere;
import fr.norsys.modele.consommateur.Personne;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static fr.norsys.modele.consommateur.Genre.FEMME;

public class JavaOld {

    public static void main(String[] args) {
        Bar bar = new Bar();
        List<Personne> personnes = bar.getPersonnes();
        List<Biere> bieres = bar.getBieres();


        System.out.println("\n\nListe des personnes dans le bar");
        for (Personne personne : personnes) {
            System.out.println(personne);
        }


        List<Personne> personnesTriees = new ArrayList<>(personnes);
        Collections.sort(personnesTriees, new Comparator<Personne>() {
            @Override
            public int compare(Personne p1, Personne p2) {
                return Integer.compare(p1.getAge(), p2.getAge());
            }
        });

        System.out.println("\n\nPersonne la plus jeune");
        System.out.println(personnesTriees.get(0));

        System.out.println("\n\nPersonne la plus agées");
        System.out.println(personnesTriees.get(personnesTriees.size() - 1));


        boolean resultat = false;
        for (Personne personne : personnes) {
            if (personne.getAge() == 19) {
                resultat = true;
                break;
            }
        }
        System.out.println("\n\nExiste t'il une personne agée de 19 ans ? " + resultat);


        System.out.println("\n\nListe des femmes dans le bar");
        for (Personne personne : personnes) {
            if (personne.getGenre() == FEMME) {
                System.out.println(personne);
            }
        }


        System.out.println("\n\nAlain consomme 1 Leffe Blonde");
        for (Biere biere : bieres) {
            if (biere.getNom().equals("Leffe Blonde")) {
                for (Personne personne : personnes) {
                    if (personne.getPrenom().equals("Alain")) {
                        personne.consomme(biere);
                        System.out.println(personne);
                        System.out.println(personne.getConsommations());
                    }
                }
            }
        }



        Biere tripleKarmeliet = null;
        Biere rinceCochon = null;
        Biere cuveeDesTrolls = null;
        Biere leffeRuby = null;

        for(Biere biere : bieres) {
            if(biere.getNom().equals("Triple Karmeliet")) {
                tripleKarmeliet = biere;
            } else if(biere.getNom().equals("Rince cochon")) {
                rinceCochon = biere;
            } else if(biere.getNom().equals("Cuvée des Trolls")) {
                cuveeDesTrolls = biere;
            } else if(biere.getNom().equals("Leffe Ruby")) {
                leffeRuby = biere;
            }
        }


        for(Personne p : personnes) {
            if(p.getPrenom().equals("Jean")) {
                p.consomme(tripleKarmeliet);
                p.consomme(tripleKarmeliet);
                p.consomme(rinceCochon);
                p.consomme(cuveeDesTrolls);
                System.out.println(p);
                System.out.println(p.getConsommations());
            }
        }

        for(Personne p : personnes) {
            if(p.getPrenom().equals("Anne")) {
                p.consomme(leffeRuby);
                p.consomme(leffeRuby);
                System.out.println(p);
                System.out.println(p.getConsommations());
            }
        }

    }
}
