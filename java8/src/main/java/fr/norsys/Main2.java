package fr.norsys;

/**
 * Created by Maquet on 29/12/2015.
 */

import fr.norsys.modele.consommateur.Personne;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import static fr.norsys.modele.consommateur.Genre.FEMME;
import static fr.norsys.modele.consommateur.Genre.HOMME;

public class Main2 {


    public static Personne trierPersonneLaPlusJeune(List<Personne> personnes) {
        Personne resultat = personnes.get(0);
        for (Personne p : personnes) {
            if (p.getAge() < resultat.getAge()) {
                resultat = p;
            }
        }
        return resultat;
    }

    public static Personne trierPersonneLaPlusAgee(List<Personne> personnes) {
        Personne resultat = personnes.get(0);
        for (Personne p : personnes) {
            if (p.getAge() > resultat.getAge()) {
                resultat = p;
            }
        }
        return resultat;
    }


    @FunctionalInterface
    public interface BinaryOp {
        abstract Personne operation(Personne x, Personne y);
    }


    public static Personne trierPersonne(List<Personne> personnes, BinaryOp op) {
        Personne resultat = personnes.get(0);
        for (Personne p : personnes) {
            resultat = op.operation(p, resultat);
        }
        return resultat;
    }


    static BinaryOp laPlusJeune = (x, y) -> {
        return x.getAge() < y.getAge() ? x : y;
    };


    static BinaryOp laPlusAgeeeee = (x, y) -> {
        return y.getAge() > x.getAge() ? y : x;
    };

    public static void main(String[] args) {


        Personne jean = new Personne("Jean", "Meurdesoif", 25, HOMME);
        Personne anne = new Personne("Anne", "Honyme", 31, FEMME);
        List<Personne> listePersonne = Arrays.asList(jean, anne);
        System.out.println("la plus jeune (sans Intefarce Fonctionnelle)");
        System.out.println(trierPersonneLaPlusJeune(listePersonne));
        System.out.println("la plus agee (sans Intefarce Fonctionnelle)");
        System.out.print(trierPersonneLaPlusAgee(listePersonne));
        System.out.println("--------------");
        System.out.println("la plus jeune (avec Intefarce Fonctionnelle)");
        System.out.println(trierPersonne(listePersonne, laPlusJeune));
        System.out.println("la plus agee (avec Intefarce Fonctionnelle)");
        System.out.println(trierPersonne(listePersonne, laPlusAgeeeee));

        // Predicate
        Predicate<Personne> estUneFille = (x) -> x.getGenre().equals(FEMME);
        System.out.println("anne est une fille :" + estUneFille.test(anne));
        System.out.println("jean est une fille :" + estUneFille.test(jean));

        // Supplier
        Supplier<Personne> fournirPersonne = Personne::new;
        System.out.println(fournirPersonne.get());

        // Consumer
        Consumer<Personne> afficherNom = (x) -> System.out.println(x.getNom());
        System.out.println("--------------------afficher chaque nom--------------------");
        for (Personne p : listePersonne) {
            afficherNom.accept(p);
        }

        // Function
        Function<Personne, String> concatNomEtPrenom = (x) -> x.getPrenom();
        System.out.println(concatNomEtPrenom.apply(jean));


        System.out.println("---avant");


        for (Personne p : listePersonne) {
            System.out.println(p);
        }

        Function<Personne, String> personnesTrigramme = (x) -> x.getPrenom().toLowerCase().charAt(0) + x.getNom().toLowerCase() + "@beer.fr";
        System.out.println(personnesTrigramme.apply(jean));


        for (Personne p : listePersonne) {
            p.setEmail(personnesTrigramme.apply(p));
        }


        System.out.println("---Apres");

        for (Personne p : listePersonne) {
            System.out.println(p);
        }





    }// fin du main


}

