package fr.norsys;

import com.sun.org.apache.xpath.internal.SourceTree;
import fr.norsys.filtres.Filtrage;
import fr.norsys.filtres.FiltreHommes;
import fr.norsys.filtres.FiltreHommesTrentenaires;
import fr.norsys.filtres.Personne;
import fr.norsys.modele.consommateur.Genre;
import fr.norsys.modele.consommateur.Genre;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

/**
 * Created by Maquet on 30/12/2015.
 */
public class Main3 {








    public static void main(String[] args) {
        Personne tom = new Personne("Tom", 23, Genre.HOMME);
        Personne fred = new Personne("Fred", 34, Genre.HOMME);
        Personne mehdi = new Personne("Mehdi", 33, Genre.HOMME);
        Personne pauline = new Personne("Pauline", 23, Genre.FEMME);
        Personne daniele = new Personne("Daniele", 28, Genre.FEMME);
        Personne marie = new Personne("Marie", 31, Genre.FEMME);
        List<Personne> listePersonne = Arrays.asList(tom, mehdi, fred, pauline, daniele, marie);


        //Chainage de Predicate
        Consumer<Personne> afficherPersonne = p -> System.out.println(p.getPrenom()+" "+p.getAge());
        Consumer<Personne> vieillirPersonne = p-> p.setAge(p.getAge()+1);

        afficherPersonne.accept(fred);


    }//fin du main


}
