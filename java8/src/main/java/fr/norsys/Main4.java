package fr.norsys;

import fr.norsys.modele.consommateur.Genre;
import fr.norsys.modele.consommateur.Personne;
import fr.norsys.modele.consommateur.Genre;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static fr.norsys.modele.consommateur.Genre.FEMME;
import static fr.norsys.modele.consommateur.Genre.HOMME;

/**
 * Created by mehdibrahmi on 09/01/2016.
 */
public class Main4 {

    public static void main(String[] args) {

        List<Personne> personnes = new ArrayList<>();
        personnes.add(new Personne("Jean", "Meurdesoif", 25, HOMME));
        personnes.add(new Personne("Anne", "Honyme", 31, FEMME));
        personnes.add(new Personne("Gérard", "Manvussa", 49, HOMME));
        personnes.add(new Personne("Alain", "Proviste", 36, HOMME));

        // moyenne d'age des hommes et des femmes
        Map<Genre, Double> averageAgeByGender = personnes.stream().collect(
                        Collectors.groupingBy(
                                Personne::getGenre,
                                Collectors.averagingInt(Personne::getAge)));

        System.out.println("Moyenne d'age des hommes " + averageAgeByGender.get(HOMME) + " ans");
        System.out.println("Moyenne d'age des femmes " + averageAgeByGender.get(FEMME) + " ans");





        double moyenneAgeHomme = personnes.stream()
                .filter(p -> HOMME.equals(p.getGenre()))
                .mapToInt(Personne::getAge)
                .average().orElseGet(() -> 0.0);

        System.out.println("Moyenne d'age des hommes " + moyenneAgeHomme + " ans");

        double moyenneAgeFemme = personnes.stream()
                .filter(p -> FEMME.equals(p.getGenre()))
                .mapToInt(Personne::getAge)
                .average().orElseGet(() -> 0.0);

        System.out.println("Moyenne d'age des femmes " + moyenneAgeFemme + " ans");
    }
}
