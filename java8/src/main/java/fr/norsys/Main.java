package fr.norsys;


import fr.norsys.modele.biere.Biere;
import fr.norsys.modele.consommateur.Genre;
import fr.norsys.modele.consommateur.Personne;
import fr.norsys.modele.consommateur.Genre;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static fr.norsys.modele.biere.Type.*;
import static fr.norsys.modele.biere.Origine.BELGIQUE;
import static fr.norsys.modele.biere.Origine.FRANCE;
import static fr.norsys.modele.consommateur.Genre.FEMME;
import static fr.norsys.modele.consommateur.Genre.HOMME;

public class Main {


    public static void main(String[] args) {

        List<Biere> bieres = new ArrayList<>();
        bieres.add(new Biere(5, BLONDE, 8, "Triple Karmeliet", BELGIQUE));
        bieres.add(new Biere(4, BLONDE, 6.6, "Leffe Blonde", BELGIQUE));
        bieres.add(new Biere(4, BRUNE, 6.5, "Leffe Brune", BELGIQUE));
        bieres.add(new Biere(4, BLONDE, 7.5, "Leffe Royale", BELGIQUE));
        bieres.add(new Biere(5, ROUGE, 5, "Leffe Ruby", BELGIQUE));
        bieres.add(new Biere(6, BLONDE, 8.5, "Rince cochon", FRANCE));
        bieres.add(new Biere(6, BLONDE, 7, "Cuvée des Trolls", BELGIQUE));

        List<Personne> personnes = new ArrayList<>();
        personnes.add(new Personne("Jean", "Meurdesoif", 25, HOMME));
        personnes.add(new Personne("Anne", "Honyme", 31, FEMME));
        personnes.add(new Personne("Gérard", "Manvussa", 49, HOMME));
        personnes.add(new Personne("Alain", "Proviste", 36, HOMME));

        System.out.println("Liste de personnes : ");
        for (Personne personne : personnes) {
            System.out.println(personne);
        }

        System.out.println("Liste de personnes : ");
        Iterator<Personne> iterator = personnes.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        System.out.println("Liste de personnes : ");
        for (int i = 0; i < personnes.size(); i++) {
            System.out.println(personnes.get(i));
        }

        System.out.println("Liste de personnes : ");
        personnes.stream().forEach(System.out::println);

        System.out.println("Liste de personnes : ");
        personnes.stream().forEach(p -> {
            System.out.println(p.getNom());
            System.out.println(p.getPrenom());
            System.out.println("--");
        });


        List<Personne> personnesTriees = new ArrayList<>(personnes);
        Collections.sort(personnesTriees, new Comparator<Personne>() {
            @Override
            public int compare(Personne p1, Personne p2) {
                return Integer.compare(p1.getAge(), p2.getAge());
            }
        });

        System.out.println("Liste de personnes triées par age: ");
        System.out.println(personnesTriees.get(0));
        System.out.println("Liste de personnes triées par age: ");
        System.out.println(personnesTriees.get(personnesTriees.size() - 1));

        System.out.println("\nPersonne la plus jeune : ");
        personnes.stream().min(Comparator.comparingInt(Personne::getAge)).ifPresent(System.out::println);

        System.out.println("\nPersonne la plus agée : ");
        personnes.stream().max(Comparator.comparingInt(Personne::getAge)).ifPresent(System.out::println);


        String reponse = (personnes.stream().filter(p -> p.getAge() == 18).findFirst().isPresent() ? "oui" : "non");
        System.out.println("Existe t'il une personne agée de 18 ans ? " + reponse);

        // Jean consomme 2 karmeliet, 1 rince cochon et 1 cuvée des trolls
        // (c'est son anniversaire et il ne conduit pas !!!)
        personnes.stream()
                .filter(p -> p.getPrenom().equals("Jean")).findFirst().get()
                .consomme(bieres.stream().filter(b -> b.getNom().equals("Triple Karmeliet")).findFirst().get())
                .consomme(bieres.stream().filter(b -> b.getNom().equals("Triple Karmeliet")).findFirst().get())
                .consomme(bieres.stream().filter(b -> b.getNom().equals("Rince cochon")).findFirst().get())
                .consomme(bieres.stream().filter(b -> b.getNom().equals("Cuvée des Trolls")).findFirst().get());


        // Alain consomme 1 Leffe Blonde
        bieres.stream().filter(b -> b.getNom().equals("Leffe Blonde")).findFirst().ifPresent(b -> {
            personnes.stream().filter(p -> p.getPrenom().equals("Alain")).findFirst().ifPresent(p -> p.consomme(b));
        });

        // Un inconnu consomme 1 bière inconnue
        bieres.stream().filter(b -> b.getNom().equals("Leffe Inconnue")).findFirst().ifPresent(b -> {
            personnes.stream().filter(p -> p.getPrenom().equals("Inconnu")).findFirst().ifPresent(p -> p.consomme(b));
        });

        // Anne consomme 2 Leffe Ruby
        personnes.stream()
                .filter(p -> p.getPrenom().equals("Anne")).findFirst().get()
                .consomme(bieres.stream().filter(b -> b.getNom().equals("Leffe Ruby")).findFirst().get())
                .consomme(bieres.stream().filter(b -> b.getNom().equals("Leffe Ruby")).findFirst().get());


        System.out.println("\nListe de personnes qui ont consommées des bières : ");
        personnes.stream().filter(p -> !p.getConsommations().isEmpty()).collect(Collectors.toList()).forEach(System.out::println);

        System.out.println("\nListe de personnes qui n'ont pas consommées de bières : ");
        personnes.stream().filter(p -> p.getConsommations().isEmpty()).collect(Collectors.toList()).forEach(System.out::println);

        System.out.println("\nPersonne la plus alcoolisé : ");
        personnes.stream()
                .max(Comparator.comparingDouble(Personne::getDegreAlcoolTotal))
                .ifPresent(System.out::println);


        System.out.println("\nPersonne la moins alcoolisé : ");
        personnes.stream()
                .min(Comparator.comparingDouble(Personne::getDegreAlcoolTotal))
                .ifPresent(System.out::println);

        System.out.println("\nPersonne qui ont consommées au moins une bière de la marque Leffe : ");
        personnes.stream().filter(p -> p.getConsommations().stream().filter(b -> b.getNom().startsWith("Leffe")).count() > 0).forEach(System.out::println);

        System.out.println("\nPersonne qui a payé le plus pour consommer des bières : ");
        personnes.stream()
                .max(Comparator.comparingDouble(Personne::getPrixConsommationsTotal))
                .ifPresent(System.out::println);

        Supplier<Personne> personneSupplier = Personne::new;

        Personne personne = personneSupplier.get();
        personne.setNom("toto");
        Personne personne2 = personneSupplier.get();
        personne2.setNom("titi");
        System.out.println(personne);
        System.out.println(personne2);


        Personne jean = personnes.stream().filter(p -> p.getPrenom().equals("Jean")).findFirst().get();

        Consumer<Personne> personneConsumer = (x) -> System.out.println(x.getNom());
        personneConsumer.accept(jean);


        Function<Personne, String> personnesEmail = (x) -> x.getPrenom().toLowerCase().charAt(0) + x.getNom().toLowerCase() + "@beer.fr";
        System.out.println(personnesEmail.apply(jean));


        //personnes.stream().map(personnesEmail).forEach(System.out::println);

        personnes.stream().peek(p -> p.setEmail(personnesEmail.apply(p))).forEach(System.out::println);


        // moyenne d'age des hommes et des femmes
        Map<Genre, Double> averageAgeByGender = personnes
                .stream()
                .collect(
                        Collectors.groupingBy(
                                Personne::getGenre,
                                Collectors.averagingInt(Personne::getAge)));

        System.out.println("Moyenne d'age des hommes " + averageAgeByGender.get(HOMME) + " ans");
        System.out.println("Moyenne d'age des femmes " + averageAgeByGender.get(FEMME) + " ans");


        personnes.stream().allMatch(p -> p.getAge() == 18);

        // Alain consomme 1 Leffe Blonde
        Predicate<Biere> isLeffeBlonde = b -> b.getNom().equals("Leffe Blonde");
        Predicate<Personne> isPersonneAlain = p -> p.getPrenom().equals("Alain");

        bieres.stream().filter(isLeffeBlonde).findFirst().ifPresent(b -> {
            personnes.stream().filter(isPersonneAlain).findFirst().ifPresent(p -> p.consomme(b));
        });


        bieres.stream().filter(isLeffeBlonde).collect(Collectors.toList());

        personnes.stream().filter(isPersonneAlain).findFirst().ifPresent(
                p -> p.setPrixTotal(p.getConsommations().stream().mapToDouble(Biere::getPrix).sum())
        );


        // calculer le prix total des bières consommées pour chaque personne
        personnes.get(0).getConsommations().stream().mapToDouble(Biere::getPrix);
        personnes.stream().forEach(p -> p.setPrixTotal(p.getConsommations().stream().mapToDouble(Biere::getPrix).sum()));
        personnes.stream().forEach(System.out::println);

        System.out.println("personne la plus dépensière");
        personnes.stream()
                .max(Comparator.comparingDouble(Personne::getPrixTotal))
                .ifPresent(System.out::println);

        // personne la moins dépensière
        System.out.println("personne la moins dépensière");
        personnes.stream()
                .min(Comparator.comparingDouble(Personne::getPrixTotal))
                .ifPresent(System.out::println);


        // tri par prix total
        System.out.println("Tri par prix total (ASC)");
        personnes.stream().sorted(Comparator.comparingDouble(Personne::getPrixTotal)).forEach(System.out::println);

        System.out.println("Tri par prix total (DESC)");
        personnes.stream().sorted(Comparator.comparingDouble(Personne::getPrixTotal).reversed()).forEach(System.out::println);


        System.out.println("Tri par prix total (ASC)");
        personnes.stream().sorted((p1, p2) -> Double.compare(p1.getPrixTotal(), p2.getPrixTotal())).forEach(System.out::println);

        System.out.println("Tri par prix total (DESC)");
        personnes.stream().sorted((p1, p2) -> Double.compare(p2.getPrixTotal(), p1.getPrixTotal())).forEach(System.out::println);




        System.out.println("Générer les adresses email pour chaque personnes");
        Predicate<Personne> prenomNomValide = p -> null != p.getPrenom() && !p.getPrenom().isEmpty() && null != p.getNom() && !p.getNom().isEmpty();
        Consumer<Personne> genererEmail = p -> p.setEmail(p.getPrenom().toLowerCase().charAt(0) + p.getNom().toLowerCase() + "@beer.fr");
        personnes.stream()
                .filter(prenomNomValide)
                .peek(genererEmail)
                .forEach(System.out::println);

        personnes.stream().parallel()
                .forEach(p -> {
                    p.setEmail(p.getPrenom().toLowerCase().charAt(0) + p.getNom().toLowerCase() + "@beer.fr");
                    System.out.println(p);
                });


        System.out.println("Liste des bières consommées");
        personnes.stream()
                .flatMap(p -> p.getConsommations().stream())
                .forEach(System.out::println);


        /*List<Biere> bieres1 = personnes.stream().flatMap(p -> p.getConsommations().stream()).collect(Collectors.toList());
        System.out.println("Nombre par type de bière consommée");
        bieres1.stream()
                .collect(Collectors.groupingBy(b -> b, Collectors.counting()))
                .forEach((biere, nombre) -> System.out.println(nombre + " " + biere));*/

        personnes.stream()
                .flatMap(p -> p.getConsommations().stream())
                .collect(Collectors.groupingBy(b -> b, Collectors.counting()))
                .forEach((biere, consommation) -> System.out.println(consommation + " " + biere));

       /* System.out.println("Liste des bières consommées");
        personnes.stream()
                .flatMap(p -> p.getConsommations().stream())
                //.collect(Collectors.groupingBy(b -> b.getNom(), Collectors.mapping((Biere b) -> b.getNom(), Collectors.toList())))
                .collect(Collectors.groupingBy(b -> b.getNom(), Collectors.counting(), Collectors.toList())))
                .forEach((nb, s) -> System.out.println( nb + " " +s));*/

    }


}
