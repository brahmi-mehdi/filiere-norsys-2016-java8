package fr.norsys.filtres;

/**
 * Created by Maquet on 31/12/2015.
 */
public interface Filtrage {
    boolean verifierPersonne(Personne personne);
}
