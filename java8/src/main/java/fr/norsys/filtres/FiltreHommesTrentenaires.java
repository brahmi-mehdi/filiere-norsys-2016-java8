package fr.norsys.filtres;

import fr.norsys.modele.consommateur.Genre;

/**
 * Created by Maquet on 31/12/2015.
 */
public class FiltreHommesTrentenaires implements Filtrage{

    @Override
    public boolean verifierPersonne(Personne personne) {
        return Genre.HOMME.equals(personne.getSexe()) && personne.getAge() > 30 && personne.getAge() < 40;
    }
}
