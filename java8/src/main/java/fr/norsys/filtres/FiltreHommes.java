package fr.norsys.filtres;

import fr.norsys.modele.consommateur.Genre;

/**
 * Created by Maquet on 31/12/2015.
 */
public class FiltreHommes implements Filtrage{
    @Override
    public boolean verifierPersonne(Personne personne) {
        return Genre.HOMME.equals(personne.getSexe());
    }
}
