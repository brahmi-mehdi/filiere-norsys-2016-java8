package fr.norsys.filtres;

import fr.norsys.modele.consommateur.Genre;
import fr.norsys.modele.consommateur.Genre;

/**
 * Created by Maquet on 30/12/2015.
 */
public class Personne {
    String prenom;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    int age;

    public Genre getSexe() {
        return genre;
    }

    public void setSexe(Genre genre) {
        this.genre = genre;
    }

    Genre genre;

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Personne(String prenom, int age, Genre genre) {
        this.prenom = prenom;
        this.age = age;
        this.genre = genre;
    }

    public String toString(){
        return "Prenom : "+this.getPrenom()+" , age : "+this.getAge()+" ("+this.getSexe()+")";
    }

}