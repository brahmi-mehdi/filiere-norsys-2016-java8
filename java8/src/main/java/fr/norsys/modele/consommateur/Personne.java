package fr.norsys.modele.consommateur;

import fr.norsys.modele.biere.Biere;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
public class Personne {

    private String prenom;

    private String nom;

    private int age;

    private Genre genre;

    private String email;

    private double prixTotal;

    private List<Biere> consommations = new ArrayList<>();

    public Personne(String prenom, String nom, int age, Genre genre) {
        this.prenom = prenom;
        this.nom = nom;
        this.age = age;
        this.genre = genre;
    }

    public Personne consomme(Biere biere) {
        getConsommations().add(biere);
        return this;
    }

    public List<Biere> getConsommations() {
        if (consommations == null) {
            consommations = new ArrayList<>();
        }
        return consommations;
    }

    public String toString() {
        return
                " [ Nom : " + this.nom +
                " | Prénom : " + this.prenom +
                " | Age : " + this.age +
                " | Genre : " + this.genre +
                " | Email : " + this.email +
                " | Nb bières : " + this.getConsommations().size() +
                " | Degre alcool total : " + this.getDegreAlcoolTotal() +
                " | Prix total : " + this.prixTotal + " ]" +
                " | Prix consommations total : " + this.getPrixConsommationsTotal() +
                " ] ";
    }


    public double getPrixConsommationsTotal() {
        //return getConsommations().stream().collect(Collectors.summingDouble(Biere::getPrix)).doubleValue();
        return getConsommations().stream().mapToDouble(Biere::getPrix).sum();
    }

    public double getDegreAlcoolTotal() {
        return getConsommations().stream().collect(Collectors.summingDouble(Biere::getDegre)).doubleValue();
    }


}
