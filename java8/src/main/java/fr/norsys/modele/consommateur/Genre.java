package fr.norsys.modele.consommateur;

import lombok.Getter;

public enum Genre {
    HOMME(1),
    FEMME(2);

    @Getter private int id;

    Genre(int id) {
        this.id = id;
    }
}
