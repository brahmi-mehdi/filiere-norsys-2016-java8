package fr.norsys.modele;

import fr.norsys.modele.biere.Biere;
import fr.norsys.modele.consommateur.Personne;
import lombok.Data;

import java.util.*;

import static fr.norsys.modele.biere.Origine.BELGIQUE;
import static fr.norsys.modele.biere.Origine.FRANCE;
import static fr.norsys.modele.biere.Type.*;
import static fr.norsys.modele.consommateur.Genre.FEMME;
import static fr.norsys.modele.consommateur.Genre.HOMME;


@Data
public class Bar {

    private List<Biere> bieres;
    private List<Personne> personnes;

    public Bar() {
        bieres = Arrays.asList(
                new Biere(5, BLONDE, 8, "Triple Karmeliet", BELGIQUE),
                new Biere(4, BLONDE, 6.6, "Leffe Blonde", BELGIQUE),
                new Biere(4, BRUNE, 6.5, "Leffe Brune", BELGIQUE),
                new Biere(4, BLONDE, 7.5, "Leffe Royale", BELGIQUE),
                new Biere(5, ROUGE, 5, "Leffe Ruby", BELGIQUE),
                new Biere(6, BLONDE, 8.5, "Rince cochon", FRANCE),
                new Biere(6, BLONDE, 7, "Cuvée des Trolls", BELGIQUE)
        );

        personnes = Arrays.asList(
                new Personne("Jean", "Meurdesoif", 25, HOMME),
                new Personne("Anne", "Honyme", 37, FEMME),
                new Personne("Gérard", "Manvussa", 49, HOMME),
                new Personne("Alain", "Proviste", 36, HOMME),
                new Personne("Sarah ", "Vigote", 23, FEMME),
                new Personne("Larry ", "Bambelle", 21, HOMME),
                new Personne("Justin ", "Ptitpeu", 19, HOMME),
                new Personne("Daisy ", "Draté", 28, FEMME)
        );
    }

}
