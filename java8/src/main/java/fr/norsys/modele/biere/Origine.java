package fr.norsys.modele.biere;

import lombok.Getter;

public enum Origine {
    FRANCE("FR"),
    BELGIQUE("BE");

    @Getter
    private String code;

    Origine(String code) {
        this.code = code;
    }
}