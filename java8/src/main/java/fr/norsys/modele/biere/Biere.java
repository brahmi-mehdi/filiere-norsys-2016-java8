package fr.norsys.modele.biere;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Biere {

    private double prix;

    private Type type;

    private double degre;

    private String nom;

    private Origine origine;

    public String toString() {
        return
                " [ Nom : " + this.nom +
                " | Degré : " + this.degre +
                " | Type : " + this.type +
                " | Origine : " + this.origine +
                " | Prix: " + this.prix +
                " ] ";
    }

}
