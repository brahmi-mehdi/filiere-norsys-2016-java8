package fr.norsys.modele.biere;

public enum Type {
    BLANCHE, BLONDE, BRUNE, NOIR, ROUGE, AMBREE;
}
