package fr.norsys.modele;

import java.util.*;

/**
 * java.util.function.Test
 * Reçoit un paramètre
 * Ne retourne rien
 * <p>
 * <p>
 * void accept(T);
 */
public class Test {


    public static void main(String[] args) {

        /**

         Consumer

         java.util.function.Test
         - Reçoit un paramètre
         - Ne retourne rien

         void accept(T);

         */
        System.out.println("### CONSUMER ###");

        Arrays.asList("a", "b", "c").forEach(System.out::println);


        /**

         Function

         java.util.function.Function
         - Reçoit un paramètre
         - Retourne une valeur

         R apply(T);

         */

        System.out.println("### FUNCTION ###");

        List<String> strings = Arrays.asList("c", "b", "a");
        strings.forEach(System.out::println);
        System.out.println("sort by name : ");
        strings.sort(Comparator.comparing(String::toLowerCase));
        strings.forEach(System.out::println);


        /**

         Supplier

         java.util.function.Supplier
         - Ne reçoit rien
         - Retourne une valeur

         T get();

         */

        System.out.println("### SUPPLIER ###");

        Random random = new Random(42);
        Optional<Integer> opt = Optional.empty();
        int value = opt.orElseGet(random::nextInt);
        System.out.println(value + "");


        /**

         Predicate

         java.util.function.Predicate
         - Reçoit un paramètre
         - Retourne un booléen

         boolean test(T);

         */

        System.out.println("### PREDICATE ###");

        List<String> strings2 = new ArrayList<String>() {{
            add("ab");
            add("bb");
            add("cb");
        }};
        strings2.forEach(System.out::println);
        strings2.removeIf(s -> s.contains("c"));
        System.out.println("sort by name : ");
        strings2.forEach(System.out::println);


    }


}
