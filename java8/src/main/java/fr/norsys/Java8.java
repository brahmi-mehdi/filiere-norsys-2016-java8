package fr.norsys;

import fr.norsys.modele.Bar;
import fr.norsys.modele.biere.Biere;
import fr.norsys.modele.consommateur.Genre;
import fr.norsys.modele.consommateur.Personne;

import java.util.Comparator;
import java.util.List;

public class Java8 {

    public static void main(String[] args) {
        Bar bar = new Bar();
        List<Personne> personnes = bar.getPersonnes();
        List<Biere> bieres = bar.getBieres();


        System.out.println("\n\nListe des personnes dans le bar");
        personnes.stream().forEach(System.out::println);


        System.out.println("\n\nPersonne la plus jeune");
        personnes.stream().min(Comparator.comparingInt(Personne::getAge)).ifPresent(System.out::println);

        System.out.println("\n\nPersonne la plus agées");
        personnes.stream().max(Comparator.comparingInt(Personne::getAge)).ifPresent(System.out::println);


        boolean resultat = personnes.stream().allMatch(p -> p.getAge() == 19);
        System.out.println("\n\nExiste t'il une personne agée de 19 ans ? " + resultat);


        System.out.println("\n\nListe des femmes dans le bar");
        personnes.stream().filter(p -> p.getGenre() == Genre.FEMME).forEach(System.out::println);


        System.out.println("\n\nAlain consomme 1 Leffe Blonde");
        bieres.stream().filter(b -> b.getNom().equals("Leffe Blonde")).findFirst().ifPresent(b -> {
            personnes.stream().filter(p -> p.getPrenom().equals("Alain")).findFirst().ifPresent(p -> {
                p.consomme(b);
                System.out.println(p);
                System.out.println(p.getConsommations());
            });
        });


        Biere tripleKarmeliet = bieres.stream().filter(b -> b.getNom().equals("Triple Karmeliet")).findFirst().get();
        Biere rinceCochon = bieres.stream().filter(b -> b.getNom().equals("Rince cochon")).findFirst().get();
        Biere cuveeDesTrolls = bieres.stream().filter(b -> b.getNom().equals("Cuvée des Trolls")).findFirst().get();
        Biere leffeRuby = bieres.stream().filter(b -> b.getNom().equals("Leffe Ruby")).findFirst().get();


        System.out.println("\n\nJean consomme 2 karmeliet, 1 rince cochon et 1 cuvée des trolls");
        personnes.stream().filter(p -> p.getPrenom().equals("Jean")).findFirst().ifPresent(p -> {
            p.consomme(tripleKarmeliet);
            p.consomme(tripleKarmeliet);
            p.consomme(rinceCochon);
            p.consomme(cuveeDesTrolls);
            System.out.println(p);
            System.out.println(p.getConsommations());
        });


        System.out.println("\n\nAnne consomme 2 Leffe Ruby");
        personnes.stream().filter(p -> p.getPrenom().equals("Anne")).findFirst().ifPresent(p -> {
            p.consomme(leffeRuby);
            p.consomme(leffeRuby);
            System.out.println(p);
            System.out.println(p.getConsommations());
        });

        // Un inconnu consomme 1 bière inconnue
        bieres.stream().filter(b -> b.getNom().equals("Leffe Inconnue")).findFirst().ifPresent(b -> {
            personnes.stream().filter(p -> p.getPrenom().equals("Inconnu")).findFirst().ifPresent(p -> p.consomme(b));
        });

        personnes.stream().forEach(
                p ->  {
                    p.setPrixTotal(p.getConsommations().stream().mapToDouble(Biere::getPrix).sum());
                    System.out.println(p);
                }
        );

    }
}
